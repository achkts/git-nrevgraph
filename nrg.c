#include <stdio.h>
#include <git2.h>
#include <string.h>

struct repo_state {
  git_repository *repo;
  git_revwalk *walker;
};

typedef struct {
    git_oid *elements;
    size_t len;
    size_t cap;
} rev_array;

void init_rev_array(rev_array *a, size_t init_size)
{
    a->elements = (git_oid*)malloc(init_size * sizeof(git_oid));
    a->len = 0;
    a->cap = init_size;
}

void free_rev_array(rev_array *a)
{
    free(a->elements);
    a->elements = NULL;
    a->len = 0;
    a->cap = 0;
}

void insert_rev(rev_array *a, git_oid el)
{
    if (a->len == a->cap) {
        a->cap *= 2;
        a->elements = (git_oid *)realloc(a->elements, a->cap * sizeof(git_oid));
    }
    a->elements[a->len++] = el;
}


// TODO: Children should be an array of nodes
// TODO: Write functions to traverse tree` (dfs)`
// TODO: construct tree while traversing from leaves (arguments) to root or
// branch points
typedef struct node {
    git_oid *oid;
    struct node *children;
    size_t nchildren;
    size_t cap;
} rev_tree_node;

void init_rev_tree(rev_tree_node *t, git_oid* n)
{
    t->oid = n;
    rev_tree_node c;
    t->children = &c;
    t->nchildren = 0;
    t->cap = 0;
}

static void cleanup(struct repo_state *rs) {
    if (rs->repo) git_repository_free(rs->repo);
    if (rs->walker) git_revwalk_free(rs->walker);
    git_libgit2_shutdown();
}

/** If an error code is passed, prints an error message, cleans up the
  * repo_state and exits. */
static void check_error(int error_code, const char *action, struct repo_state *rs)
{
  const git_error *error = giterr_last();
  if (!error_code)
    return;

  printf("Error %d %s - %s\n", error_code, action,
      (error && error->message) ? error->message : "???");
  cleanup(rs);
  exit(1);
}

static void printobj(const git_oid* oid, const char *name)
{
    char out[GIT_OID_HEXSZ+1];
    git_oid_fmt(out, oid);
    out[GIT_OID_HEXSZ] = '\0';
    printf("%s: %s\n", name, out);
    return;
}

static void printcommit(git_commit* commit, const char *name)
{
    char shortid[8] = {0};
    git_oid_tostr(shortid, 7, git_commit_id(commit));
    printf("[%s] %s", shortid, git_commit_summary(commit));
    if (strcmp(name, "")) {
        printf(" (%s)", name);
    }
    printf("\n");
    return;
}

static const git_oid* parse_revstr(const char *revstr, struct repo_state *rs)
{
    git_revspec revspec;
    int error = git_revparse(&revspec, rs->repo, revstr);
    check_error(error, "rev parse", rs);
    return git_object_id(revspec.from);
}

static int init_repo_state(struct repo_state *rs)
{
    git_libgit2_init();
    // find repository root from CWD
    git_buf gbuf = {0};
    int error = git_repository_discover(&gbuf, ".", 0, NULL);
    check_error(error, "discover repository", rs);
    error = git_repository_open(&rs->repo, gbuf.ptr);
    check_error(error, "opening repository", rs);
    git_buf_free(&gbuf);
    error = git_revwalk_new(&rs->walker, rs->repo);
    check_error(error, "rev_count: walker setup", rs);
    return 0;
}

static int rev_count(const git_oid *start, const git_oid *stop, struct repo_state *rs)
{
    git_revwalk_hide(rs->walker, start);
    git_revwalk_push(rs->walker, stop);

    int count = 0;
    for (git_oid next; !git_revwalk_next(&next, rs->walker); count++);
    git_revwalk_reset(rs->walker);
    return count;
}

static int walk_print(const git_oid *start, const git_oid *stop, const git_oid mb_oids[], int nmb, char *names[], struct repo_state *rs)
{
    git_revwalk_hide(rs->walker, start);
    git_revwalk_push(rs->walker, stop);

    int count = 0;
    for (git_oid next; !git_revwalk_next(&next, rs->walker); count++) {
        // check if next is in mb_oids
        for (int idx = 0; idx < nmb; idx++) {
            if (git_oid_equal(&next, &mb_oids[idx])) {
                char hash[5];
                git_oid_nfmt(hash, 5, &next);
                printf("---[%3d]---%s", count, hash);
                count = 0;
                break;
            }
        }
    }
    printf("---[%3d]---", count);
    git_revwalk_reset(rs->walker);
    return 0;
}


// Retrieves a commit object from an oid
static git_commit* get_commit(git_oid oid, struct repo_state rs)
{
    git_commit *commit;
    int error = git_commit_lookup(&commit, rs.repo, &oid);
    check_error(error, "get_commit", &rs);
    return commit;
}

static int in(const git_oid obj, rev_array *allrevs)
{
    for (int idx = 0; idx < allrevs->len; idx++) {
        if (git_oid_equal(&obj, &allrevs->elements[idx])) {
            return 1;
        }
    }
    return 0;
}

static int collectrevs(const git_oid *start, rev_array *allrevs, struct repo_state *rs)
{
    git_revwalk_reset(rs->walker);
    git_revwalk_push(rs->walker, start);
    /* git_revwalk_hide(rs->walker, &allrevs->elements[0]); */
    /* printf("Hiding "); */
    /* git_commit *mb = get_commit(allrevs->elements[0], *rs); */
    /* printcommit(mb, "MergeBase"); */

    /* for (int idx = 0; idx < allrevs->len; idx++) { */
    /*     git_revwalk_hide(rs->walker, &allrevs->elements[idx]); */
    /* } */

    int count = 0;
    int ok = 0;
    for (git_oid next; !git_revwalk_next(&next, rs->walker); count++) {
        git_commit *commit = get_commit(next, *rs);
        printf("%d: ", count);
        printcommit(commit, "");
        if (in(next, allrevs)) {
            printf("Found known commit after %d\n", count);
            printcommit(commit, "|-");
            ok = 1;
            break;
        }
        insert_rev(allrevs, next);
    }
    if (!ok) {
        printf("Run through\n");
    }
    printf("Revs found: %zu\n", allrevs->len);
    return count;
}


int main(int argc, char *argv[])
{
    printf("\n===========\n BEGIN NRG \n===========\n\n");
    if (argc == 1) {
        printf("Need at least one revision");
        exit(1);
    }

    struct repo_state rs = {0, 0};
    init_repo_state(&rs);

    int nrevs = argc-1;
    git_oid revids[nrevs];
    git_commit *commits[nrevs];
    char *revnames[nrevs];
    printf(":: Commit objects for arguments:\n");
    for (int idx = 1; idx < argc; idx++) {
        revnames[idx-1] = argv[idx];
        git_oid oid = *parse_revstr(revnames[idx-1], &rs);
        /* printobj(&oid, argv[idx]); */
        revids[idx-1] = oid;
        commits[idx-1] = get_commit(oid, rs);
        printcommit(commits[idx-1], revnames[idx-1]);
    }
    printf("\n");

    // find the merge base for all given revisions
    git_oid mb_oid;
    int error = git_merge_base_many(&mb_oid, rs.repo, nrevs, revids);
    check_error(error, "merge base many", &rs);
    /* printobj(&mb_oid, "Merge base"); */

    git_commit* mergebase = get_commit(mb_oid, rs);
    printf(":: Found merge base:\n");
    printcommit(mergebase, "MergeBase");

    rev_array allrevs;
    init_rev_array(&allrevs, 1);
    rev_tree_node root;
    init_rev_tree(&root, &mb_oid);
    insert_rev(&allrevs, mb_oid);
    for (int idx = 0; idx < nrevs; idx++) {
        printf(":: Walking\n");
        printcommit(commits[idx], revnames[idx]);
        collectrevs(&revids[idx], &allrevs, &rs);
        printf("----\n");
    }

    free_rev_array(&allrevs);
    cleanup(&rs);
    return 0;
}
